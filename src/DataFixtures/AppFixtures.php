<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Level;
use App\Entity\TeamMate;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $beginner = new Level();
        $beginner->setName('Débutant');
        $manager->persist($beginner);

        $intermediaire = new Level();
        $intermediaire->setName('Intermediaire');
        $manager->persist($intermediaire);

        $expert = new Level();
        $expert->setName('Expert');
        $manager->persist($expert);

        for ($i = 0; $i < 50; $i++) {
            $teammate = new TeamMate();

            $male = $faker->firstNameMale();
            $female = $faker->firstNameFemale();
            $teammate->setName($faker->randomElement([$male, $female]));

            if ($teammate->getName() === $male) {
                $teammate->setIsMale(true);
            } else {
                $teammate->setIsMale(false);
            }

            $teammate->setIsAvailable(true);

            $teammate->setAge($faker->numberBetween($min = 17, $max = 85));
            $teammate->setCity($faker->city());
            $teammate->setLevel($faker->randomElement([$beginner, $intermediaire, $expert]));
            $manager->persist($teammate);
        }

        $manager->flush();
    }
}
