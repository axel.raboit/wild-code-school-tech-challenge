<?php

namespace App\Controller;

use App\Entity\TeamMate;
use App\Form\TeamMateType;
use App\Repository\TeamMateRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class TeamMateController extends AbstractController
{
    /**
     * @Route("/", name="team_mate_index", methods={"GET"})
     */
    public function index(TeamMateRepository $teamMateRepository): Response
    {
        $crew = $teamMateRepository->findBy(['isAvailable' => false]);
        $crewNumber = 0;
        foreach ($crew as $person) {
            if ($person->getIsAvailable() === false) {
                $crewNumber += 1;
            }
        }

        return $this->render('team_mate/index.html.twig', [
            'team_mates' => $teamMateRepository->findAll(),
            'crewNumber' => $crewNumber,
            'crew' => $crew
        ]);
    }

    /**
     * @Route("/nouveau", name="team_mate_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $teamMate = new TeamMate();
        $form = $this->createForm(TeamMateType::class, $teamMate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($teamMate);
            $entityManager->flush();

            return $this->redirectToRoute('team_mate_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('team_mate/new.html.twig', [
            'team_mate' => $teamMate,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="team_mate_show", methods={"GET"})
     */
    public function show(TeamMate $teamMate): Response
    {
        return $this->render('team_mate/show.html.twig', [
            'team_mate' => $teamMate,
        ]);
    }

    /**
     * @Route("/recrutement/{id}", name="team_mate_recruit", methods={"GET"})
     */
    public function recruit(
        $id,
        EntityManagerInterface $entityManager,
        TeamMateRepository $teamMateRepository
    ): Response {

        $teamMate = $teamMateRepository->find($id);
        $crew = $teamMateRepository->findBy(['isAvailable' => false]);
        $crewNumber = 0;
        foreach ($crew as $person) {
            if ($person->getIsAvailable() === false) {
                $crewNumber += 1;
            }
        }

        $teamMate->setIsAvailable(false);
        $entityManager->flush();

        return $this->redirectToRoute('team_mate_index', [], Response::HTTP_SEE_OTHER);

        return $this->render('team_mate/index.html.twig', [
            'team_mates' => $teamMateRepository->findAll(),
            'crewNumber' => $crewNumber,
            'crew' => $crew
        ]);
    }

    /**
     * @Route("/desembauche/{id}", name="team_mate_fire", methods={"GET"})
     */
    public function fire($id, EntityManagerInterface $entityManager, TeamMateRepository $teamMateRepository): Response
    {
        $teamMate = $teamMateRepository->find($id);

        $crew = $teamMateRepository->findBy(['isAvailable' => false]);
        $crewNumber = 0;
        foreach ($crew as $person) {
            if ($person->getIsAvailable() === false) {
                $crewNumber += 1;
            }
        }

        $teamMate->setIsAvailable(true);
        $entityManager->flush();

        return $this->redirectToRoute('team_mate_index', [], Response::HTTP_SEE_OTHER);

        return $this->render('team_mate/index.html.twig', [
            'team_mates' => $teamMateRepository->findAll(),
            'crewNumber' => $crewNumber,
            'crew' => $crew
        ]);
    }

    /**
     * @Route("/{id}", name="team_mate_delete", methods={"POST"})
     */
    public function delete(Request $request, TeamMate $teamMate, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $teamMate->getId(), $request->request->get('_token'))) {
            $entityManager->remove($teamMate);
            $entityManager->flush();
        }

        return $this->redirectToRoute('team_mate_index', [], Response::HTTP_SEE_OTHER);
    }
}
