<?php

namespace App\Form;

use App\Entity\Level;
use App\Entity\TeamMate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class TeamMateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('age', IntegerType::class, [
                'label' => 'Age'
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville'
            ])
            ->add('isMale', ChoiceType::class, [
                'label' => 'Genre',
                'choices'  => [
                    'Homme' => true,
                    'Femme' => false,
                ],
            ])
            ->add('level', EntityType::class, [
                'class' => Level::class,
                'choice_label' => 'name',
                'label' => 'Niveau'
            ])
            ->add('isAvailable', ChoiceType::class, [
                'label' => 'Recruter',
                'choices'  => [
                    'Oui' => false,
                    'Non' => true,
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TeamMate::class,
        ]);
    }
}
