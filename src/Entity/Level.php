<?php

namespace App\Entity;

use App\Repository\LevelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LevelRepository::class)
 */
class Level
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=TeamMate::class, mappedBy="level")
     */
    private $teamMates;

    public function __construct()
    {
        $this->teamMates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|TeamMate[]
     */
    public function getTeamMates(): Collection
    {
        return $this->teamMates;
    }

    public function addTeamMate(TeamMate $teamMate): self
    {
        if (!$this->teamMates->contains($teamMate)) {
            $this->teamMates[] = $teamMate;
            $teamMate->setLevel($this);
        }

        return $this;
    }

    public function removeTeamMate(TeamMate $teamMate): self
    {
        if ($this->teamMates->removeElement($teamMate)) {
            // set the owning side to null (unless already changed)
            if ($teamMate->getLevel() === $this) {
                $teamMate->setLevel(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
