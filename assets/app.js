import './styles/app.scss';

// start the Stimulus application
import './bootstrap';

const imagesContext = require.context('../assets/images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);


