<?php

namespace App\Tests;

use App\Entity\Level;
use PHPUnit\Framework\TestCase;

class LevelUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $level = new Level();

        $level->setName('name');

        $this->assertTrue($level->getName() === 'name');
    }

    public function testIsFalse()
    {
        $level = new Level();

        $level->setName('name');

        $this->assertFalse($level->getName() === 'false');
    }

    public function testIsEmpty()
    {
        $level = new Level();

        $this->assertEmpty($level->getName());
    }
}

