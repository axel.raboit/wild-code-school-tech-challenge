<?php

namespace App\Tests;

use App\Entity\TeamMate;
use PHPUnit\Framework\TestCase;

class TeamMateUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $teamMate = new TeamMate();

        $teamMate->setName('name')
             ->setAge(30)
             ->setCity('city')
             ->setIsMale(true)
             ->setIsAvailable(true)
        ;

        $this->assertTrue($teamMate->getName() === 'name');
        $this->assertTrue($teamMate->getAge() === 30);
        $this->assertTrue($teamMate->getCity() ===  'city');
        $this->assertTrue($teamMate->getIsMale() === true);
        $this->assertTrue($teamMate->getIsAvailable() === true);
    }

    public function testIsFalse()
    {
        $teamMate = new TeamMate();

        $teamMate->setName('name')
             ->setAge(30)
             ->setCity('city')
             ->setIsMale(true)
             ->setIsAvailable(true)
        ;

        $this->assertFalse($teamMate->getName() === 'false');
        $this->assertFalse($teamMate->getAge() === false);
        $this->assertFalse($teamMate->getCity() ===  'false');
        $this->assertFalse($teamMate->getIsMale() === 'false');
        $this->assertFalse($teamMate->getIsAvailable() === 'false');
    }

    public function testIsEmpty()
    {
        $teamMate = new TeamMate();

        $this->assertEmpty($teamMate->getName());
        $this->assertEmpty($teamMate->getAge());
        $this->assertEmpty($teamMate->getCity());
        $this->assertEmpty($teamMate->getIsMale());
        $this->assertEmpty($teamMate->getIsAvailable());
    }
}

